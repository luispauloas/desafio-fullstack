﻿using System;
using System.Data.SqlClient;

namespace QueueTest
{
    public class Billing
    {

        //Criar uma camada de model (Models) e inserir uma interface
        //uma interface para ser o contrato da class,
        //na qual teria as assinaturas do metodos

        #region Interface

        //public string Add()

        #endregion

        //Dentro da camada de model (Models) e inserir esse techo em

        //ex name: IBillingModel'

        #region Model

        public Guid Id { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }

        #endregion

        public string Add()
        {
            //₢riar uma camada de business (Business) 
            //e inserir dentro de uma class com name:'BillingBO'
            //na qual estaria toda a regra de negocio do metodo em questão
            #region Business

            if (!string.IsNullOrEmpty(Description))
                return "Description is required";

            if (Amount is 0)
                return "Amount is required";

            #endregion

            //Criar uma camada para acesso ao banco de dados (Repositories)
            //Inserir dentro de uma class com name 'BillingRepository'
            //na qual toda comunicação com banco será feita
            //outra possibilidade seria implementar a tecnlogia entity framework
            //para fazer o mapemento das tabelas e entidades de forma entity to entity
            #region Repository
            using (var connection = new SqlConnection("myTestConnection")) //os daddos de conexão com banco devem vir do web confing por meio de importação de configuração
            {

                string query = "INSERT INTO BILLING (DESCRIPTION, AMOUNT) VALUES (@description, @amount)";

                //implementar o try() catch()
                var cmd = new SqlCommand(query, connection);
                cmd.Parameters.AddWithValue("description", Description);
                cmd.Parameters.AddWithValue("amount", Amount);

                connection.Open();
                cmd.ExecuteNonQuery();

                //seria necessario implementar a funcão connection.close() e connection.dispose() 
                //dessa form fechando a conexão com o banco e liberando a memoria que foi alocada para tal
                //no servidor
            }

            #endregion

            return "Billing created";
        }
    }
}
