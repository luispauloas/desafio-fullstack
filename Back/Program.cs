﻿using System;
using System.Collections.Generic;


namespace QueueTest
{
    class Program
    {
        static void Main(string[] args)
        {

            Queue<int> que = new Queue<int>();


            que.Enqueue(11);
            que.Enqueue(10);
            que.Enqueue(12);

            Console.WriteLine("Removido: {0}", que.Dequeue());
        }
}
}
