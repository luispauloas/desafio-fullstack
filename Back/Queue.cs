﻿using System;
using System.Collections.Generic;

namespace QueueTest
{
    public class Queue<T>
    {
    private  Stack<T> _stack1  = new Stack<T>();
    private Stack<T> _stack2 = new Stack<T>();


        public void Enqueue(T item)
        {
            try
            {
                _stack1.Push(item);
                
            }
            catch (Exception exception)
            {
                throw exception;
            }

        }
        public T Dequeue()
        {
            int size = _stack1.Count;
            T result;
            try
            {
                if (size>0)
                {
                     result =  _stack1.Pop();
                }
                else
                {
                    throw new Exception("A fila está vazia");
                }
            }
            catch (Exception exception)
            {

                throw exception;
            }
            return result;
        }
    }
}
