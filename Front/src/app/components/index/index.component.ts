import { Component, OnInit } from '@angular/core';
import { CoinsService } from 'src/app/services/coins.service';

import { ICoin } from "../../models/icoins"

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {
  coins: ICoin[] = []
  isRequestFailed = false
  isRequestOnProgress = false;
  isSmallDevice = false


  constructor(private coinsService: CoinsService) { }

  ngOnInit(): void {
    this.isSmallDevice = window.innerWidth < 1024
    window.addEventListener("resize", () => {
      this.isSmallDevice = window.innerWidth < 1024
    })
  }


  loadCoins(): void {
    this.isRequestOnProgress = true
    this.isRequestFailed = false,
    this.coinsService.get().subscribe(
      (coins: ICoin[]) => this.coins = coins,
      (error) => {
        this.isRequestFailed = true
        this.isRequestOnProgress = false
      },
      () => this.isRequestOnProgress = false
    )
    }
}
