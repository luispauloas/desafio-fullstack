export interface ICoin {
  id: Number,
  symbol: String,
  name: String
}