import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { ICoin } from '../models/icoins';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CoinsService {
  baseURL = "https://api.coingecko.com/api/v3/coins/list"
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  constructor(private httpClient: HttpClient) { }

  get(): Observable<ICoin[]> {
    return this.httpClient.get<ICoin[]>(this.baseURL, this.httpOptions)
  }
}
